package gofilescontainer

import "errors"

var (
	ErrSectionAlreadyAdded     = errors.New("section already added")
	ErrSectionIsntAdded        = errors.New("section isn't added")
	ErrSectionIsInvalid        = errors.New("section is invalid")
	ErrYouReachedFileSizeLimit = errors.New("you reached filesize limit")
)
