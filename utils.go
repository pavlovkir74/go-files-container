package gofilescontainer

import (
	"os"
	"strings"
	"time"
)

// |> 1. Settings

const (
	DefaultWriteFilemode   = 0644
	DefaultMaxSize         = 10 * MB
	DefaultFileCleanEvery  = 2 * time.Minute
	DefaultFileMaxLifeSpan = 60 * time.Hour

	// Filesizes
	KB = 1024
	MB = KB * 1024
)

// |> 2. Internal

func valideDir(path string) error {
	// Попытаться получить статус директории
	_, err := os.Stat(path)

	// Если директории нет, создаем
	if os.IsNotExist(err) {
		return os.MkdirAll(path, 0755)
	} else if err != nil {
		return err
	} else {
		return nil
	}
}

// |> 3. External

func GetFileExtension(filename string) string {
	splited := strings.Split(filename, ".")
	if len(splited) == 2 {
		return splited[1]
	} else {
		return ""
	}
}
