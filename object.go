package gofilescontainer

import (
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"sync"
	"time"

	"github.com/google/uuid"
)

// |> 1. Construct

type FileContainer struct {
	// configure it out
	path          string
	sections      map[string]*FileSection
	sectionsMutex sync.RWMutex
	// app parts
	logger LoggerInterface
	// settings
	FileMaxSize int
}

func NewFileContainer(path string, logger LoggerInterface, sections ...FileSectionSettings) (*FileContainer, error) {
	// validate logger
	if logger == nil {
		logger = &EmptyLogger{}
	}
	// make container and validate it
	container := &FileContainer{
		// configure it out
		path:     path,
		sections: make(map[string]*FileSection),
		// app parts
		logger: logger,
		// settings
		FileMaxSize: DefaultMaxSize,
	}
	err := valideDir(path)
	if err != nil {
		logger.Errorw(
			"validation of file container work dir error",
			"dir", path,
			"error", err.Error(),
		)
		return nil, err
	}
	// setup sections
	for _, section := range sections {
		// setup all sections
		err = container.SetupSection(container.path, section)
		if err != nil {
			logger.Errorw(
				"setup section error",
				"section", section.Name,
				"error", err.Error(),
			)
			return nil, err
		} else {
			logger.Infow(
				"setup section successfully",
				"section", section.Name,
			)
		}
	}
	// ok
	logger.Infow(
		"setup container successfully",
		"dir", path,
	)
	return container, nil
}

type FileSection struct {
	// configure it out
	path     string
	settings FileSectionSettings
	// cleaning
	cleaningStop chan struct{}
	// app parts
	logger LoggerInterface
}

type FileSectionSettings struct {
	// configure it out
	Name string
	// Cleaning
	EnableCleaning, CleanAllOnStart bool
	CleanEvery, FilesLifeSpan       time.Duration
}

func NewFileSection(containerPath string, settings FileSectionSettings, logger LoggerInterface) (*FileSection, error) {
	section := &FileSection{
		path:     path.Join(containerPath, settings.Name),
		settings: settings,
		logger:   logger,
	}
	return section, section.setup()
}

type TempFile struct {
	// configure it out
	path string
	file *os.File
}

func NewTempFile(dir, name string) (*TempFile, error) {
	// create
	file, err := os.CreateTemp(dir, name)
	if err != nil {
		return nil, err
	}
	// make and return
	return &TempFile{
		path: path.Join(dir, name),
		file: file,
	}, nil
}
func EmptyTempFile() *TempFile {
	return &TempFile{}
}

// |> 2. Inteface

// |> 2.1. Container

// sections
func (fc *FileContainer) SetupSection(containerPath string, settings FileSectionSettings) error {
	// mutex
	fc.sectionsMutex.Lock()
	defer fc.sectionsMutex.Unlock()
	// check having section
	if _, ok := fc.sections[settings.Name]; ok {
		return ErrSectionAlreadyAdded
	}
	// create section
	section, err := NewFileSection(containerPath, settings, fc.logger)
	if err != nil {
		return err
	} else {
		fc.sections[settings.Name] = section
		return nil
	}
}
func (fc *FileContainer) UnSetupSection(sectionName string) error {
	// mutex
	fc.sectionsMutex.Lock()
	defer fc.sectionsMutex.Unlock()
	// check having section
	if section, ok := fc.sections[sectionName]; ok {
		// delete
		delete(fc.sections, sectionName)
		// unsetup and return ok
		section.unsetup()
		return nil
	} else {
		return ErrSectionIsntAdded
	}
}
func (fc *FileContainer) DestroySection(sectionName string) error {
	// mutex
	fc.sectionsMutex.Lock()
	defer fc.sectionsMutex.Unlock()
	// check having section
	if section, ok := fc.sections[sectionName]; ok {
		// delete
		delete(fc.sections, sectionName)
		// unsetup and return
		return section.destroy()
	} else {
		return ErrSectionIsntAdded
	}
}
func (fc *FileContainer) HasSection(name string) bool {
	// mutex
	fc.sectionsMutex.RLock()
	defer fc.sectionsMutex.RUnlock()
	// check having section
	_, ok := fc.sections[name]
	return ok
}

// life cycle
func (fc *FileContainer) Shutdown() {
	// mutex
	fc.sectionsMutex.Lock()
	defer fc.sectionsMutex.Unlock()
	// shuwdown sections
	for _, section := range fc.sections {
		// unsetup
		section.unsetup()
	}
	// clear
	fc.sections = make(map[string]*FileSection)
}

// files
// files, internal
func (fc *FileContainer) getUUID() string {
	return uuid.New().String()
}
func (fc *FileContainer) makeFilename(ext string) string {
	uuid := fc.getUUID()
	if ext == "" {
		return uuid
	}
	return fmt.Sprintf("%v.%v", uuid, ext)
}

// files, external
func (fc *FileContainer) SaveFileBytes(bytes []byte, sectionName, ext string) (filename string, err error) {
	// validate input
	if len(bytes) > fc.FileMaxSize {
		return filename, ErrYouReachedFileSizeLimit
	}
	if !fc.HasSection(sectionName) {
		return filename, ErrSectionIsInvalid
	}
	// make filename and path to store
	filename = fc.makeFilename(ext)
	// save bytes and return
	return filename, os.WriteFile(fc.GetSectionFilename(sectionName, filename), bytes, DefaultWriteFilemode)
}
func (fc *FileContainer) SaveFileReadCloser(rc io.ReadCloser, sectionName, ext string) (filename string, err error) {
	// read
	bytes, err := io.ReadAll(rc)
	if err != nil {
		return filename, err
	}
	// validate input
	if len(bytes) > fc.FileMaxSize {
		return filename, ErrYouReachedFileSizeLimit
	}
	if !fc.HasSection(sectionName) {
		return filename, ErrSectionIsInvalid
	}
	// make filename and path to store
	filename = fc.makeFilename(ext)
	// save bytes and return
	return filename, os.WriteFile(fc.GetSectionFilename(sectionName, filename), bytes, DefaultWriteFilemode)
}
func (fc *FileContainer) GetSectionFilename(section, filename string) string {
	return path.Join(fc.path, section, filename)
}
func (fc *FileContainer) MakeTempFile(sectionName, ext string) (file *TempFile, err error) {
	if !fc.HasSection(sectionName) {
		return nil, ErrSectionIsInvalid
	}
	// make dir and filename
	filename := fc.makeFilename(ext)
	dir := path.Join(fc.path, sectionName)
	// save bytes and return
	return NewTempFile(dir, filename)
}

// |> 2.2. Section

// setup
func (fs *FileSection) setup() (err error) {
	// clean if it needs (apply setting)
	if fs.settings.CleanAllOnStart {
		os.RemoveAll(fs.path)
	}
	// validate dir
	err = valideDir(fs.path)
	if err != nil {
		// failed
		return err
	} else {
		// ok
		fs.runCleaning()
		return nil
	}
}
func (fs *FileSection) unsetup() {
	// just stop cleaning
	fs.stopCleaning()
}
func (fs *FileSection) destroy() (err error) {
	// stop cleaning
	fs.stopCleaning()
	// remove all in folder
	return os.RemoveAll(fs.path)
}

// cleaning
func (fs *FileSection) runCleaning() {
	// validate settings
	if !fs.settings.EnableCleaning {
		return
	}
	if fs.settings.CleanEvery <= 0 {
		fs.settings.CleanEvery = DefaultFileCleanEvery
	}
	if fs.settings.FilesLifeSpan <= 0 {
		fs.settings.FilesLifeSpan = DefaultFileMaxLifeSpan
	}
	// make a ticker
	ticker := time.NewTicker(fs.settings.CleanEvery)
	fs.cleaningStop = make(chan struct{})
	// async ticker loop
	go func() {
		// Ticker loop
		for {
			select {
			case <-ticker.C:
				fs.clean()
			case <-fs.cleaningStop:
				ticker.Stop()
				return
			}
		}
	}()
}
func (fs *FileSection) stopCleaning() {
	if fs.cleaningStop != nil {
		select {
		case fs.cleaningStop <- struct{}{}:
		default:
		}
	}
}
func (fs *FileSection) clean() {
	// log
	fs.logger.Infow(
		"cleaning expired files starting..",
		"section", fs.settings.Name,
	)

	// Save starting time
	start := time.Now()

	// Use filepath.Walk for handle files
	err := filepath.Walk(fs.path, func(filename string, info os.FileInfo, err error) error {
		// error
		if err != nil {
			return err
		}

		// skip self dir
		if info.Name() == fs.settings.Name {
			return nil
		}

		// check date
		lastAccess := info.ModTime()
		sinceLastAccess := time.Since(lastAccess)
		if sinceLastAccess >= fs.settings.FilesLifeSpan {
			// remove file
			err := os.Remove(filename)
			if err != nil {
				fs.logger.Warnw(
					"error on clearing file",
					"section", fs.settings.Name,
					"filename", filename,
					"dir", fs.path,
					"error", err.Error(),
				)
			}
		}

		return nil
	})

	// calculate perfoming time
	delay := time.Since(start)

	// log
	if err != nil {
		fs.logger.Warnw(
			"error on walking the file tree",
			"section", fs.settings.Name,
			"dir", fs.path,
			"error", err.Error(),
		)
	} else {
		fs.logger.Infow(
			"cleaning expired files finished",
			"section", fs.settings.Name,
			"dir", fs.path,
			"delay", delay,
		)
	}
}

// |> 2.3. Temp file
func (tf *TempFile) GetFile() *os.File {
	return tf.file
}
func (tf *TempFile) GetPath() string {
	return tf.path
}
func (tf *TempFile) Destroy() error {
	if tf == nil {
		return nil
	}
	// try to close
	if tf.file != nil {
		tf.file.Close()
	}
	// just remove file
	if tf.path != "" {
		return os.Remove(tf.path)
	}
	return nil
}
