package gofilescontainer

type LoggerInterface interface {
	Infow(msg string, fields ...interface{})
	Warnw(msg string, fields ...interface{})
	Errorw(msg string, fields ...interface{})
}

type EmptyLogger struct{}

func (e *EmptyLogger) Infow(msg string, fields ...interface{})  {}
func (e *EmptyLogger) Warnw(msg string, fields ...interface{})  {}
func (e *EmptyLogger) Errorw(msg string, fields ...interface{}) {}
